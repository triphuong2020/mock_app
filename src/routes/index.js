
// Pages;
import Login from "../pages/Login/index.jsx";
import Register from "../pages/Register/index.jsx";
import Home from "../pages/Home/index.jsx";
import config from "../config/index.js";

// Public routes
const publicRoutes = [
    { path: config.home, component: Home, layout: true},
    { path: config.login, component: Login },
    { path: config.register, component: Register },
];

const privateRoutes = [];

export { publicRoutes, privateRoutes };
